//WAP to find the distance between two points using structures and 4 functions.

#include <stdio.h>
#include <math.h>

struct Point {
    float x1,x2,y1,y2;
}point1,point2;


float input()
{
    printf("For The First point:\n");
    printf("Enter The Value for X:\n");
    scanf("%f",&point1.x1);
    printf("Enter the Value For Y:\n");
    scanf("%f",&point1.y1);
    
    printf("For The Second point:\n");
    printf("Enter the Value For X:\n");
    scanf("%f",&point2.x2);
    printf("Enter the Value For Y:\n");
    scanf("%f",&point2.y2);
}


float display(float firstPoint,float secondPoint,float Distance)
{
    printf("The distance between the two points %f and %f is: %f\n", firstPoint, secondPoint,Distance);
}


float output()
{
    float firstPoint,secondPoint,distance;
    firstPoint = point2.x2 - point1.x1;
    secondPoint = point2.y2 - point1.y1;
    distance = sqrt(firstPoint*firstPoint + secondPoint*secondPoint);
    display(firstPoint,secondPoint,distance);
    return distance;
}


int main()
{
    input();
    output();
    
    return 0;
}


