//Write a C program to add 2 numbers and print the sum.

#include<stdio.h>

int main()
{
	int sum, a, b;
	printf("Enter the two numbers to perform addition:");
	scanf("%d %d", &a, &b);

	sum = a + b;

	printf("The sum of two numbers %d and %d is: %d ", a, b, sum);
	return 0;

}
